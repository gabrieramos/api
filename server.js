//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get("/clientes/:idCliente", function(req, res){
  res.send('Aqui tiene al cliente No. :'+ req.params.idCliente);
});

app.post("/", function(req, res){
  res.send('Hemos recibido su petición cambiada');
});

app.put("/", function(req, res){
  res.send('Hemos recibido su petición d eput');
});

app.delete("/", function(req, res){
  res.send('Hemos recibido su petición de delete');
});
